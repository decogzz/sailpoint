import requests
import datetime
import os
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from urllib.parse import urlparse

# GitHub API URL
github_api_url = "https://api.github.com"

# Email configuration (modify as needed)
sender_email = "decogzzfaceit@gmail.com"  # Sender's email address
recipient_email = os.getenv("RECIPIENT_EMAIL", "")  # Read the recipient_email variable
email_subject = "GitHub Pull Request Summary"

def extract_github_repo(url):
    """
    Extract the owner and repository name from a GitHub repository URL.
    Example URL: https://github.com/owner/repo
    """
    parsed_url = urlparse(url)
    path_segments = parsed_url.path.split("/")
    if len(path_segments) == 3:
        owner, repo = path_segments[1], path_segments[2]
        return owner, repo
    return None, None

def get_pull_requests(owner, repo):
    # Calculate the date one week ago from today
    one_week_ago = datetime.datetime.now() - datetime.timedelta(days=7)
    formatted_date = one_week_ago.strftime("%Y-%m-%d")

    # GitHub API endpoint to retrieve pull requests
    endpoint = f"{github_api_url}/repos/{owner}/{repo}/pulls"
    
    # Query parameters to filter pull requests
    params = {
        "state": "all",
        "sort": "created",
        "direction": "desc",
        "since": formatted_date,
    }

    try:
        response = requests.get(endpoint, params=params)
        response.raise_for_status()
        return response.json()
    except requests.exceptions.HTTPError as e:
        print(f"Error: {e}")
        return []

def send_email(email_subject, email_body):
    # Email configuration (modify as needed)
    smtp_server = "smtp.gmail.com"  # SMTP server for your email provider
    smtp_port = 587  # Port for the SMTP server
    smtp_username = "decogzzfaceit@gmail.com"  # Your email username
    smtp_password = os.getenv("APP_PASSWORD")  # Your app password

    # Create the email message
    message = MIMEMultipart()
    message["From"] = sender_email
    message["To"] = recipient_email
    message["Subject"] = email_subject

    # Attach the email body
    message.attach(MIMEText(email_body, "plain"))

    # Establish a connection to the SMTP server
    server = smtplib.SMTP(smtp_server, smtp_port)
    server.starttls()

    # Login to your email account
    server.login(smtp_username, smtp_password)

    # Send the email
    server.sendmail(sender_email, recipient_email, message.as_string())

    # Close the SMTP server connection
    server.quit()

def main():
    github_url = os.getenv("GITHUB_URL", "")  # Read the GITHUB_URL variable
    owner, repo = extract_github_repo(github_url)
    
    if not owner or not repo:
        print("Invalid GitHub repository URL.")
        return
    
    pull_requests = get_pull_requests(owner, repo)
    
    if not pull_requests:
        print("No pull requests found.")
        return
    
    print("Pull Request Summary:")
    
    email_body = "Pull Request Summary:\n\n"
    
    for pr in pull_requests:
        email_body += f"Title: {pr['title']}\n"
        email_body += f"State: {pr['state']}\n"
        email_body += f"Author: {pr['user']['login']}\n"
        email_body += f"Created At: {pr['created_at']}\n"
        email_body += f"Updated At: {pr['updated_at']}\n\n"
        
    # Send the email with the summary
    send_email(email_subject, email_body)

    # Print the results to the pipeline log
    print(email_body)
        
if __name__ == "__main__":
    main()
